#!/bin/bash
#
# Configure Kubernetes on this Ubuntu/Mint machine as a master
# Version 0.0.1
# Author: David JACQUENS <david A-T jacquens.com>


#Variables
#for exemple, network="10.1.2.0/24"
network="10.1.4.0/24"


if [ -z $network ]
then
    echo "Variable network must be set before running this script."
    exit 2;
fi


#We must not be admin to run this script
if [ "$EUID" -eq 0 ]
  then echo "Please do not run as root"
  exit 3;
fi

sudo kubeadm init --pod-network-cidr=$network
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

#Install Flannel
kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml

echo
echo
echo "Cluster configured."