#!/bin/bash
#
# Install Kubernetes on an Ubuntu/Mint machine
# Version 0.0.1
# Author: David JACQUENS <david A-T jacquens.com>
# Copyright (c) 2019  David JACQUENS


#We need to be admin to run this script
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 3;
fi

#Install curl if it is not already installed.
apt update
command -v curl >/dev/null 2>/dev/null || apt install curl -y

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
add-apt-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
apt-get update
apt-get install -y kubelet kubeadm kubectl


echo
echo
echo "Kubeadm, kubectl and kubelet installed on this machine."