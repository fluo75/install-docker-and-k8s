#!/bin/bash
#
# Install Docker on an Ubuntu/Mint machine
# Version 0.0.1
# Author: David JACQUENS <david A-T jacquens.com>
# Copyright (c) 2019  David JACQUENS

#
#
#
#
#
#
#
#
#
#
# TO DO: modify how we check for which version of Ubuntu/Mint we are using with the command 'grep "UBUNTU_CODENAME=" /etc/os-release | cut -d= -f2'
#
#
#
#
#
#
#
#
#

#We take a single parameter, this is to choose if we want:
# STABLE: the latest stable version 
# LATEST: or the latest full featured version

#Variables
version=$1
version=$(echo $version | tr '[:upper:]' '[:lower:]' )


if [ -z $version ]
then 
    echo "Error, you must specify a parameter 'STABLE' or 'LATEST' to choose what version you want.";
    exit 2;
fi


#We need to be admin to run this script
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 3;
fi
apt update
command -v curl >/dev/null 2>/dev/null || apt install curl -y

#We retrieve the laster version of Compose and Machine. OK, this is a very crude way of doing it... :)
composeversion=$(curl -s https://github.com/docker/compose/releases/latest | cut -d/ -f8 | cut -d\" -f1) #need to be improved 'cause could be invalid in future release
machineversion=$(curl -s https://github.com/docker/machine/releases/latest | cut -d/ -f8 | cut -d\" -f1)


if [ 'stable' = $version ]
then
    echo "Installing Docker with the Stable version."
    apt-get remove docker docker-engine docker.io containerd runc -y
    apt-get update
    apt-get install apt-transport-https ca-certificates  curl gnupg-agent software-properties-common -y
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - 

    osversion=$(lsb_release -cs)
    #if we are on Mint, the osversion will be wrong... :(
    case $osversion in
        tara|tessa|tina|tricia)
            osversion='bionic' ;;
        sarah|serena|sonya|sylvia)
            osversion='xenial' ;;
        cindy)
            osversion='strech' ;;
        ulyana)
            osversion='focal' ;;
    esac
    echo "OS version $osversion"
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $osversion stable"
    apt-get update
    apt-get install docker-ce docker-ce-cli containerd.io -y

elif [ 'latest' = $version ]
then
    echo "Installing Docker with the Latest version."
    echo

    apt update
    apt install curl -y
    mkdir /tmp/installdocker
    cd /tmp/installdocker
    echo "Launching the install of docker. This will take a while."
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh

else
    echo "Unknow argument."
    echo "Error, you must specify a parameter 'STABLE' or 'LATEST' to choose what version you want.";
    exit 3;
fi


#Let's install Docker Machine & Docker Compose
#Docker Machine
curl -L https://github.com/docker/machine/releases/download/$machineversion/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine &&
    chmod +x /tmp/docker-machine &&
    cp /tmp/docker-machine /usr/local/bin/docker-machine
#Docker Compose
curl -L "https://github.com/docker/compose/releases/download/$composeversion/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo 
echo
echo "Docker installed."
echo 
echo
echo "Installed: $(docker --version)"
echo
if [ -z $USER ]
then
    echo "You may wish to add the command 'usermod -aG docker $USER', so you won't have to use sudo with docker command."
    echo "This is easiest in test environment but not enough secure for production environment."
    echo
    echo
fi
echo
echo "!!! Docker Machine & Docker Compose will not get updated automatically, you need to think updating Docker once in a while."

